package pl.polsl.et.app.services;


import com.google.common.hash.Hashing;

import java.nio.charset.StandardCharsets;

public class HashProcessService implements ProcessService {

    @Override
    public String processMeasured(Integer count, String value) {
        String response = null;
        for (int i = 0; i < count; i++) {
            response = Hashing.sha256()
                    .hashString(value, StandardCharsets.UTF_8)
                    .toString();
        }
        return "{ \"response\": " + response + " }";
    }
}

