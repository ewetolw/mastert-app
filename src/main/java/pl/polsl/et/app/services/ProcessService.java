package pl.polsl.et.app.services;

public interface ProcessService {
    String processMeasured(Integer count, String value) throws InterruptedException;
}
