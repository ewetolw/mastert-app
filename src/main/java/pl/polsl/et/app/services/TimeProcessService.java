package pl.polsl.et.app.services;

import java.util.concurrent.TimeUnit;

public class TimeProcessService implements ProcessService {
    @Override
    public String processMeasured(Integer count, String value) throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(count);
        return value;
    }
}
