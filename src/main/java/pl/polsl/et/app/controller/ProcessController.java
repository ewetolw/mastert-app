package pl.polsl.et.app.controller;

import io.micrometer.core.instrument.MeterRegistry;

import io.micrometer.core.instrument.Timer;
import io.prometheus.client.CollectorRegistry;
import io.prometheus.client.Histogram;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.polsl.et.app.services.ProcessService;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

@RestController
public class ProcessController {

    public Timer timer;
    public MeterRegistry registry;
    public Histogram requestDuration;
    @Autowired
    public ProcessService service;

    public ProcessController(MeterRegistry meterRegistry, CollectorRegistry collectorRegistry) {
        requestDuration = Histogram.build()
                .name("process_req_duration")
                .help("Time for HTTP request.")
                .linearBuckets(
                        Double.parseDouble(System.getenv("histStart"))/1000,
                        Double.parseDouble(System.getenv("histWidth"))/1000,
                        Integer.parseInt(System.getenv("histCount")))
                .register(collectorRegistry);
        timer = Timer.builder("process")
                .publishPercentileHistogram()
                .minimumExpectedValue(Duration.ofMillis(Long.parseLong(System.getenv("histStart"))))
                .maximumExpectedValue(Duration.ofMillis(
                        (Long.parseLong(System.getenv("histCount")) -1 )
                        * Long.parseLong(System.getenv("histWidth"))
                        + Long.parseLong(System.getenv("histStart"))
                ))
                .tag("response", HttpStatus.OK.toString())
                .register(meterRegistry);
        registry = meterRegistry;
    }

    @GetMapping("/process_t")
    public ResponseEntity<String> processWithTimer(
            @RequestParam(name = "count") Integer count,
            @RequestParam(name = "value") String value
    ) {
        Timer.Sample sample = Timer.start(registry);
        ResponseEntity<String> response;
        Timer timerResult = null;
        try {
            response = ResponseEntity.ok(service.processMeasured(count, value));
            timerResult = registry.timer("process", "response", response.getStatusCode().toString());
        } catch (Exception e) {
            response = ResponseEntity.badRequest().body("Error" + e.getMessage());
            timerResult = registry.timer("process", "response", response.getStatusCode().toString());
        } finally {
            long durationInNs = sample.stop(timerResult);
            System.out.println(TimeUnit.NANOSECONDS.toMillis(durationInNs));
        }
        return response;
    }

    @GetMapping("/process_h")
    public ResponseEntity<String> processWithHist(
            @RequestParam(name = "count") Integer count,
            @RequestParam(name = "value") String value
    ) {
        Histogram.Timer timer = requestDuration.startTimer();
        ResponseEntity<String> response;
        try {
            response = ResponseEntity.ok(service.processMeasured(count, value));
        } catch (Exception e) {
            response = ResponseEntity.badRequest().body("Error" + e.getMessage());
        } finally {
            double responseTime = timer.observeDuration();// time in sec
            System.out.println(responseTime);
        }
        return response;
    }
}
