package pl.polsl.et.app;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.polsl.et.app.services.HashProcessService;
import pl.polsl.et.app.services.ProcessService;
import pl.polsl.et.app.services.TimeProcessService;


@Configuration
public class Config {

    @Bean
    public ProcessService getHashService() throws Exception {
        switch (System.getenv("serviceType")) {
            case "hash":
                return new HashProcessService();
            case "time":
                return new TimeProcessService();
            default:
                throw new Exception("unsupported service type");
        }
    }

}

